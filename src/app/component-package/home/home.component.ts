import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  show:boolean = true;

  slideConfig = {
    dots: true,
    infinite: true,
    speed: 500,
    arrows: false,
    fade: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: 'linear',
    adaptiveHeight: true
   
  };
  
  constructor() { }

  ngOnInit(): void {
  }
  
  showSocial(){
    this.show = !this.show;
  }
}
