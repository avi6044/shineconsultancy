import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { NavbarComponent } from '../navbar/navbar.component';

@Component({
  selector: 'app-main-component',
  templateUrl: './main-component.component.html',
  styleUrls: ['./main-component.component.scss']
})
export class MainComponentComponent implements OnInit {

  @ViewChild(NavbarComponent, { read: true, static: false }) navbar: NavbarComponent;
  constructor(@Inject(DOCUMENT) private document: Document,private element: ElementRef) { }

  ngOnInit(): void {
  }
  @HostListener('window:scroll', [])
  onWindowScroll() {
   var navbar: HTMLElement = this.element.nativeElement.children[0].children[0];
    if (document.body.scrollTop > 60 ||     
    document.documentElement.scrollTop > 60) {
      navbar.classList.remove('navbar-transparent');
    }else{
      navbar.classList.add('navbar-transparent');
    }
  }
}
